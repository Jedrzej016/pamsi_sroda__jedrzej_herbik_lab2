#include <iostream>
#include <string>
#include "kolejka.hh"

class komorka{
public:
  std::string imie;
  int wiek;
  komorka(){wiek=0;}
};

using namespace std;

int dl_kolejki=0;
ostream& operator << (ostream& StrmWy,const komorka &WyswietlanaWartosc);
istream& operator >> (istream& StrmWy,komorka &dane);
bool wyswietl(List<komorka> &dane);
bool usun(List<komorka> &lista,int nr);

int main(int argc, char *argv[]){
  Kolejka<komorka> kolejka;                            // korzen listy
  char wybor=0;                                 // zmienna wyboru
  int nr;                                       // zmienna elementu
  komorka tmp;
  while(1){
    cout<<endl;                                 // menu wyboru
    cout<<"Obsluga kolejki osob."<<endl;
    cout<<"Wybierz co chcesz robic:"<<endl;
    cout<<"1. Dodaj do kolejki element"<<endl;
    cout<<"2. Wyswietl kolejke"<<endl;
    cout<<"3. Usun z kolejki element"<<endl;
    cout<<"4. Usun z kolejki wskazany element"<<endl;
    cout<<"5. Wyczysc kolejke"<<endl;
    cout<<"q. zamknij"<<endl;
    cin>>wybor;
    switch(wybor){
    case '1':
      cin>>tmp;
      kolejka.dodaj(tmp);
      break;
    case '2':
      wyswietl(kolejka);
      break;
    case '3':
      try{
	cout<<"Usunieto: "<<kolejka.usun()<<endl;
      }catch(ListEmptyException){
	cout<<"Kolejka jest pusta"<<endl;
      }
      break;
    case '4':
      cout<<"Ktory element chcesz usunac: ";
      cin>>nr;
      usun(kolejka,nr);
      break;
    case '5':
      kolejka.clear();
      cout<<endl<<"Kolejka zostala oprozniona"<<endl;
      break;
    case 'q':
      kolejka.clear(); // zwalnianie pamieci przed zamknieciem
      cout<<"dl: "<<kolejka.rozmiar()<<endl<<"Is empty: "<<kolejka.isEmpty()<<endl;
      cout<<"Wyciek pamieci: "<<dl_kolejki<<endl;
      return 0;
      break;
    default:
      cout<<"Zle okreslony wybor"<<endl;
      break;
    }
  }
}

ostream& operator << (ostream& StrmWy,const komorka &WyswietlanaWartosc){                         // wyswietlanie komorek
  std::cout<<"Imie: "<<WyswietlanaWartosc.imie<<endl<<"Wiek: "<<WyswietlanaWartosc.wiek<<endl;
  return StrmWy;
}

istream& operator >> (istream& StrmWy,komorka &dane){                                             // inicjalizacja komorek
  std::cout<<std::endl;
  std::string napis;
  std::cout<<"Podaj imię: ";
  std::cin>>dane.imie;
  std::cout<<"Podaj wiek: ";
  std::cin>>dane.wiek;
  std::cout<<std::endl;
  return StrmWy;  
}

bool wyswietl(List<komorka> &dane){
  List<komorka> tmp;
  int rozmiar=dane.rozmiar();
  if(dane.isEmpty()){
    cout<<endl<<"Kolejka jest pusta"<<endl;
    return false;
  }
  for(int i=0;i<rozmiar;i++){                        // przeniesienie poprzedzajacych elementow
    cout<<dane.pierwszy()<<endl;                     // wyswietlenie 
    tmp.dodaj(dane.usun());
  }
  for(int i=0;i<rozmiar;i++){                        // nalozenie poprzednich elementow spowrotem
    dane.dodaj(tmp.usun());
  }
  tmp.clear();
  return true;
}

bool usun(List<komorka> &kolejka,int nr){
  List<komorka> tmp;
  if(nr>kolejka.rozmiar()){
    cout<<"podano element nie nalezacy do kolejki."<<endl<<"Kolejka ma "<<kolejka.rozmiar()<<" elementow"<<endl;
    return false;
  }
  for(int i=1;i<nr;i++){                                // zdjecie poprzedzajacych elementow
    tmp.dodaj(kolejka.usun());
  }
  try{                                                  // usuniecie wskazanego elementu
    cout<<"Usunieto: "<<endl<<kolejka.usun()<<endl;
  }catch(ListEmptyException){
    cout<<"Kolejka jest pusta"<<endl;
  }    
  for(int i=1;i<nr;i++){                                // nalozenie poprzednich elementow spowrotem
    kolejka.dodaj(tmp.usun());
  }
  return true;
}
