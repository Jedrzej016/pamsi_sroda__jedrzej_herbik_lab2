
#include <iostream>
#include<string>


using namespace std;

void permutacje(string ciag,int liczba);
bool jestPal(string testStr);
void zamien(char &a,char &b){char c=a;a=b;b=c;}
void usunDup();
void wyswietl_liste();
int Silnia(int x);
string *palList=NULL;
int dl=0;
int licznik=0;

int main(int argc, char *argv[]){
  string ciag_znakow;
  cout<<endl<<"Podaj ciag znakow: ";
  cin>>ciag_znakow;
  dl=ciag_znakow.length();
  palList=new string[Silnia(dl)]; // gromadzi palindromy
  permutacje(ciag_znakow,0);      // wywolanie permutacji
  //  wyswietl_liste();
  usunDup();                      // usuniecie powtorzen
  wyswietl_liste();
  delete[] palList;               // zwolnienie pamieci
  return 0;
}

bool jestPal(string testStr){
  int dl=testStr.length();
  if(dl==1)return true;  // wyraz o jednym znaku jest palindromem
  else if(dl==2){        // sprawdza czy znaki sa identyczne, jezeli tak to jest to palindrom
      if((testStr[0]==testStr[1])||(testStr[0]==testStr[1]+32)||(testStr[0]==testStr[1]-32))return true;
      else return false;
    }
  if(!((testStr[0]==testStr[dl-1])||(testStr[0]==testStr[dl-1]+32)||(testStr[0]==testStr[dl-1]-32)))return false; // sprawdza boczne znaki
  if(!(jestPal(testStr.substr(1,dl-2))))return false; // rekurencyjne przekazanie podzbioru
  return true;
  // funkcja zwraca true, nie tylko gdy wszystkie litery lezace w rownej ogleglosci od srodka slowa sa rowne (np. 'a' oraz 'a'),
  // funkcja uznaje rowniez za identyczne litery male i duze (np. 'A' oraz 'a'), dlatego jest tak rozbudowany warunek funkcji if 
}

void wyswietl_liste(){
  cout<<endl;
  for(int i=0;i<Silnia(dl);i++){
    if(palList[i].length())cout<<palList[i]<<" ";
  }
  cout<<endl;
}

void usunDup(){                  // usuwa powtorzenia na liscie
  string pomocniczy;
  for(int i=0;i<Silnia(dl);i++){    // przeszukule liste palindromow
    pomocniczy=palList[i];          // przechowuje zawartosc w zmiennej pomocniczej
    for(int j=0;j<Silnia(dl);j++){
      if(j!=i)if(pomocniczy==palList[j])palList[j].clear(); // dla calej listy porownoje z zmienna pomocnicza
    }
  }
}

void permutacje(string ciag,int liczba){
  if(liczba<dl-1){
    for(int i=liczba;i<dl;i++){
      zamien(ciag[liczba],ciag[i]);   // przestawienie elementow
      permutacje(ciag,liczba+1);      // rekurencyjne wywolanie
      zamien(ciag[liczba],ciag[i]);   // przestawienie elementow
    }
  }
  else{
    //    cout<<ciag<<endl;
    if(jestPal(ciag)){
      palList[licznik]=ciag;          // dolozenie do listy palindromow
      licznik++;
    }
  }
}

int Silnia(int x){
  if(!x)return 1;
  else return x*Silnia(x-1);
}
