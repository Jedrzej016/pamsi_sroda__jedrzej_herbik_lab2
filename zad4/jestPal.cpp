#include <iostream>
#include <string>

using namespace std;

#include "Kolejka.hh"
#include "Deque.hh"
#include "jestPal.hh"

bool przypisz_napis(string napis,Deque<char> &dane){
  int dl_slowa=napis.length();
  for(int i=0;i<dl_slowa;i++)dane.wstaw(napis[i]); // przekazanie zawartosci string do kolejki
  if(dane.rozmiar_d()!=dl_slowa){ // gdy dlugosc strig'a rozna od dlugosci kolejki to zwroc false
    if(dane.clear_d())cout<<endl<<"Wyciek pamieci: "<<dl_kolejki<<endl; // awaryjne zwolnienie pamieci
    return false;
  }
  return true; // gdy przekazywanie zakonczone powodzeniem zrwor true
}
/*
bool jestPal(Deque<char> &dane){ // nie rekurencyjnie
  int dl=dane.rozmiar();
  for(int i=0;i<dl/2;i++){ // petla dla polowy dlugosci
    if(!((dane.pierwszy()==dane.ostatni())||(dane.pierwszy()==dane.ostatni()+32)||(dane.pierwszy()==dane.ostatni()-32))){ // sprawdzenie skrajnych elementow
      if(dane.clear())cout<<endl<<"Wyciek pamieci: "<<dl_kolejki<<endl; // jezeli skrajne elementy sa rozne zwolnienie pamieci
      return false;
    }else{ // jezeli skrajne elementy sa identyczne to je usun
      try{ // usuniete z powodzeniem 
	dane.usunPierwszy();
      }catch(DequeEmptyException){ // blad przy usuwaniu. nie ma co usuwac
	cout<<"Kolejka jest pusta"<<endl;	
      }
      try{
	dane.usunOstatni();
      }catch(DequeEmptyException){
	cout<<"Kolejka jest pusta"<<endl;	
      }
    }
  }
  if(dane.clear())cout<<endl<<"Wyciek pamieci: "<<dl_kolejki<<endl; // awaryjne zwolnienie pamieci 
  return true;
  // funkcja zwraca true, nie tylko gdy wszystkie litery lezace w rownej ogleglosci od srodka slowa sa rowne (np. 'a' oraz 'a'),
  // funkcja uznaje rowniez za identyczne litery male i duze (np. 'A' oraz 'a'), dlatego jest tak rozbudowany warunek funkcji if 
}
*/
bool jestPal(Deque<char> &dane){ // rekurencyjnie
  int dl=dane.rozmiar_d();
  if(dl==0){ // jezeli nie ma elementow zwroc true
    if(dane.clear_d())cout<<endl<<"Wyciek pamieci: "<<dl_kolejki<<endl; // awaryjne zwolnienie pamieci
    return true;
  }
  else if(dl==1){ // jezeli jest jeden element zwroc true
    if(dane.clear_d())cout<<endl<<"Wyciek pamieci: "<<dl_kolejki<<endl; // awaryjne zwolnienie pamieci
    return true;
  }
  else if(dl==2){ // jezeli sa dwa elementy to je porownaj i zwroc true jesli jednakowe ablo false jezeli rozne
    if((dane.pierwszy_d()==dane.ostatni_d())||(dane.pierwszy_d()==dane.ostatni_d()+32)||(dane.pierwszy_d()==dane.ostatni_d()-32)){
      if(dane.clear_d())cout<<endl<<"Wyciek pamieci: "<<dl_kolejki<<endl; // awaryjne zwolnienie pamieci
      return true;
    }else{
      if(dane.clear_d())cout<<endl<<"Wyciek pamieci: "<<dl_kolejki<<endl; // awaryjne zwolnienie pamieci
      return false;
    }
  }
  if(!((dane.pierwszy_d()==dane.ostatni_d())||(dane.pierwszy_d()==dane.ostatni_d()+32)||(dane.pierwszy_d()==dane.ostatni_d()-32))){
    if(dane.clear_d())cout<<endl<<"Wyciek pamieci: "<<dl_kolejki<<endl; // awaryjne zwolnienie pamieci
    return false;
  }
  try{ // jezeli skrajne elementy sa identyczne to je usun 
    dane.usunPierwszy();
  }catch(DequeEmptyException){ // blad przy usuwaniu. nie ma co usuwac
    cout<<"Kolejka jest pusta"<<endl;	
  }
  try{
    dane.usunOstatni();
  }catch(DequeEmptyException){
    cout<<"Kolejka jest pusta"<<endl;
  }
  
  if(!(jestPal(dane))){ // rekurencyjne wywolanie
    if(dane.clear_d())cout<<endl<<"Wyciek pamieci: "<<dl_kolejki<<endl; // awaryjne zwolnienie pamieci
    return false;
  }
  if(dane.clear_d())cout<<endl<<"Wyciek pamieci: "<<dl_kolejki<<endl; // awaryjne zwolnienie pamieci
  return true;
  // funkcja zwraca true, nie tylko gdy wszystkie litery lezace w rownej ogleglosci od srodka slowa sa rowne (np. 'a' oraz 'a'),
  // funkcja uznaje rowniez za identyczne litery male i duze (np. 'A' oraz 'a'), dlatego jest tak rozbudowany warunek funkcji if 
}
