#include <iostream>
#include <string>

int dl_kolejki=0; // zmienna do okreslania przecieku pamieci

using namespace std;

#include "Kolejka.hh"     // biblioteka kolejki oraz listy
#include "Deque.hh"     // biblioteka kolejki 
#include "jestPal.hh"   // biblioteka do palindromow i przypisu ciagu znakow do kolejki


int main(int argc, char *argv[]){
  Deque<char> dane; // klasa zawierajaca wskazniki na kolejke
  string wyraz;     // zmienna odbierajaca dane z wejsca
  while(1){
    cout<<"podaj wyraz: ";
    cin>>wyraz;
    if(przypisz_napis(wyraz,dane)){  // przepisanie ciagu znakow do kolejki i sprawdzenie jej poprawnosci
      if(jestPal(dane))cout<<"Wyraz "<<wyraz<<" jest palindromem"<<endl;     // sprawdzenei polindromu, wywolanie funkcji
      else cout<<"Wyraz "<<wyraz<<" nie jest palindromem"<<endl;
    }
    if(dane.clear_d())cout<<"Nie zwolniono calej pamieci"<<endl; // awaryjne zwolnienie pamieci wraz z infromacja na wypadek nie zwolnienia 
    //    cout<<endl<<"Wyciek pamieci: "<<dl_kolejki<<endl; // kontrola wycieku pamieci
    cout<<endl;
  }
  if(dane.clear_d())cout<<"Nie zwolniono calej pamieci"<<endl; // awaryjne zwolnienie pamieci wraz z infromacja na wypadek nie zwolnienia 
  cout<<endl<<"Wyciek pamieci: "<<dane.rozmiar_d()<<endl<<"Wyciek pamieci: "<<dl_kolejki<<endl; // kontrola wycieku pamieci
  return 0;
}
