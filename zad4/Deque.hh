class DequeEmptyException{};

template <class Object> // szablon Deque
class Deque: public List<Object>, public Kolejka<Object>{
  Object zawartosc;
protected:
  int dl_deque;  
public:
  Deque(){this->head=NULL;this->begin=NULL;this->end=NULL;dl_deque=0;};
  int rozmiar_d() const;
  bool isEmpty_d() const; 
  bool clear_d();         // wyczysc
  Object& pierwszy_d() throw(DequeEmptyException);
  Object& ostatni_d() throw(DequeEmptyException);
  Object& wstaw(const Object& obj);
  Object& usunPierwszy() throw(DequeEmptyException);
  Object& usunOstatni() throw(DequeEmptyException);
};

template <class Object>
int Deque<Object>::rozmiar_d() const{
  return dl_deque;
}

template <class Object>
bool Deque<Object>::isEmpty_d() const{
  if(dl_deque) return false;
  return true;
}

template <class Object>
Object& Deque<Object>::pierwszy_d() throw(DequeEmptyException){
  return this->pierwszy_k(); // pierwszy element kolejki
}

template <class Object>
Object& Deque<Object>::ostatni_d() throw(DequeEmptyException){
  return this->pierwszy(); // pierwszy element listy
}

template <class Object>
Object& Deque<Object>::wstaw(const Object& obj){
  this->dodaj(obj);   // wstawia do listy
  this->dodaj_k(obj); // wstawia do kolejki
  dl_deque++;
  zawartosc=obj;
  return zawartosc;
}

template <class Object>
Object& Deque<Object>::usunPierwszy() throw(DequeEmptyException){
  try{
    zawartosc=this->usun_k(); // usuwanie z kolejki
    dl_deque--;
  }catch(ListEmptyException){
    std::cout<<"Kolejka jest pusta"<<std::endl;
    this->clear(); // czysci liste
  }  
  return zawartosc;
}

template <class Object>
Object& Deque<Object>::usunOstatni() throw(DequeEmptyException){
  try{
    zawartosc=this->usun(); // usuwanie z listy
    dl_deque--;
  }catch(ListEmptyException){
    std::cout<<"Kolejka jest pusta"<<std::endl;
    this->clear_k();  // czysci kolejke
  }  
  return zawartosc;
}

template <class Object>
bool Deque<Object>::clear_d(){
  this->clear();   // czysci liste
  this->clear_k(); // czysci kolejke
  dl_deque=0;
  if((this->head==NULL)&&(this->begin==NULL)&&(this->end==NULL)){
    //    std::cout<<"Kolejka jest pusta"<<std::endl; // kontrola dzialania funkcji
    return true;
  }
  return false;
}
