class ListEmptyException{};
extern int dl_kolejki; // zmienna do okreslania przecieku pamieci

template <class Object>
class wezelListy{
protected:
  Object zawartosc;    // dane
  wezelListy<Object> *next; // wskaznik do przodu
public:
  wezelListy();
  ~wezelListy();
  Object& getZawartosc();
  Object& setZawartosc(const Object &dane);
  wezelListy<Object>*& getNext();
  wezelListy<Object>*& setNext(wezelListy<Object> (*dane));
};

template <class Object>
Object& wezelListy<Object>::getZawartosc(){return zawartosc;}

template <class Object>
Object& wezelListy<Object>::setZawartosc(const Object &dane){zawartosc=dane;return zawartosc;}

template <class Object>
wezelListy<Object>*& wezelListy<Object>::getNext(){return next;}

template <class Object>
wezelListy<Object>*& wezelListy<Object>::setNext(wezelListy<Object> (*dane)){next=dane;return next;}

template <class Object>
wezelListy<Object>::~wezelListy(){dl_kolejki--;}

template <class Object>
wezelListy<Object>::wezelListy(){next=NULL;dl_kolejki++;}

template <class Object> // szablon listy
class List{
protected:
  Object zawartosc;    // dane
  wezelListy<Object> *head; // wskaznik do przodu  
  int dl;
public:
  List();
  int rozmiar() const;
  bool isEmpty() const; 
  bool clear();         // wyczysc
  Object& pierwszy() throw(ListEmptyException);
  Object& dodaj(const Object& obj);
  Object& usun() throw(ListEmptyException);
};

template <class Object>
List<Object>::List(){
  head=NULL;
  dl=0;
}

template <class Object>
int List<Object>::rozmiar() const{
  return dl;
}

template <class Object>
bool List<Object>::isEmpty() const{
  if(dl) return false;
  return true;
}

template <class Object>
bool List<Object>::clear(){
  wezelListy<Object> *tmp=NULL;
  while(head!=NULL){ // dopoki sa elementy
    tmp=head->getNext();  // przejscie na nastepne elementy
    //    cout<<"Usunieto znak: "<<next->zawartosc<<endl; // kontrola dzialania funkcji
    delete(head); // zaolnienie pamieci
    head=tmp; // przesuniecie korzenia
    dl=0;
  }
  if(head==NULL){
    //    std::cout<<"Lista jest pusta"<<std::endl; // kontrola dzialania funkcji
    return true;
  }
  return false;
}

template <class Object>
Object& List<Object>::pierwszy() throw(ListEmptyException){
  if(head!=NULL){
    return head->getZawartosc(); // zwrocenie wartosci pierwszego elementu
  }else{
    //cout<<"Lista jest pusta"<<endl;
    throw ListEmptyException(); // zrzucenie bledu
  }
}

template <class Object>
Object& List<Object>::dodaj(const Object& obj){
  wezelListy<Object> *tmp=NULL;
  tmp=head;
  head=new wezelListy<Object>;   // alokacja nowego wezla
  head->setZawartosc(obj);       // inicjalizacja nowego wezla
  dl++;    
  head->setNext(tmp);            // dolozenie do listy
  return head->getZawartosc();
}

template <class Object>
Object& List<Object>::usun() throw(ListEmptyException){
  wezelListy<Object> *tmp=NULL;
  if(head!=NULL){ // gdy lista zawiera elementy
    tmp=head->getNext();
    //    cout<<"Usunieto znak: "<<next->zawartosc<<endl;
    zawartosc=head->getZawartosc();
    delete(head); // zwolnienie pamieci
    head=tmp;
    dl--;
    return zawartosc;
  }else{ // gdy lista pusta
    //cout<<"lista jest pusta"<<endl;
    throw ListEmptyException(); // zrzucenie bledu 
  }
}

template <class Object> // szablon kolejki
class Kolejka{
protected:
  Object zawartosc;    // dane
  int dl;  
  wezelListy<Object> *begin; // wskaznik do przodu  
  wezelListy<Object> *end; // wskaznik do przodu  
public:
  Kolejka();
  int rozmiar_k() const;
  bool isEmpty_k() const; 
  bool clear_k();         // wyczysc
  Object& pierwszy_k() throw(ListEmptyException);
  Object& dodaj_k(const Object& obj);
  Object& usun_k() throw(ListEmptyException);
};


template <class Object>
Object& Kolejka<Object>::usun_k() throw(ListEmptyException){
  wezelListy<Object> *tmp=NULL;
  if(begin!=NULL){ // gdy kolejka zawiera elementy
    tmp=begin->getNext();
    //    cout<<"Usunieto znak: "<<next->zawartosc<<endl;
    this->zawartosc=begin->getZawartosc();
    delete(begin); // zwolnienie pamieci
    begin=tmp;
    this->dl--;
    if(begin==NULL)end=NULL;
    return this->zawartosc;
  }else{ // gdy kolejka pusta
    //cout<<"Kolejka jest pusta"<<endl;
    throw ListEmptyException(); // zrzucenie bledu 
  }
}

template <class Object>
Object& Kolejka<Object>::dodaj_k(const Object& obj){
  wezelListy<Object> *tmp=new wezelListy<Object>;
  tmp->setZawartosc(obj);
  if(begin==NULL){ // gdy kolejka pusta
    begin=tmp;
  }else{          // gdy kolejka zawiera elementy
    end->setNext(tmp);
  }
  end=tmp; // wprzypisanie pamieci do wskaznika koncowego
  this->dl++;
  return end->getZawartosc();
}


template <class Object>
Kolejka<Object>::Kolejka(){
  begin=NULL;
  dl=0;
}

template <class Object>
int Kolejka<Object>::rozmiar_k() const{
  return dl;
}

template <class Object>
bool Kolejka<Object>::isEmpty_k() const{
  if(dl) return false;
  return true;
}

template <class Object>
bool Kolejka<Object>::clear_k(){
  wezelListy<Object> *tmp=NULL;
  while(begin!=NULL){ // dopoki sa elementy
    tmp=begin->getNext();  // przejscie na nastepne elementy
    //    cout<<"Usunieto znak: "<<next->zawartosc<<endl; // kontrola dzialania funkcji
    delete(begin); // zaolnienie pamieci
    begin=tmp; // przesuniecie korzenia
    dl=0;
  }
  if(begin==NULL){
    //    std::cout<<"Kolejka jest pusta"<<std::endl; // kontrola dzialania funkcji
    return true;
  }
  return false;
}

template <class Object>
Object& Kolejka<Object>::pierwszy_k() throw(ListEmptyException){
  if(begin!=NULL){
    return begin->getZawartosc(); // zwrocenie wartosci pierwszego elementu
  }else{
    //cout<<"Kolejka jest pusta"<<endl;
    throw ListEmptyException(); // zrzucenie bledu
  }
}

