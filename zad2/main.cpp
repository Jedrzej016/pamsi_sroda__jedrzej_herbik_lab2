#include <iostream>
#include <string>
#include "lista.hh"

class komorka{
public:
  std::string imie;
  int wiek;
  komorka(){wiek=0;}
};

using namespace std;

int dl_kolejki=0;
ostream& operator << (ostream& StrmWy,const komorka &WyswietlanaWartosc);
istream& operator >> (istream& StrmWy,komorka &dane);
bool wyswietl(List<komorka> &dane);
bool usun(List<komorka> &lista,int nr);

int main(int argc, char *argv[]){
  List<komorka> lista;                            // korzen listy
  char wybor=0;                                 // zmienna wyboru
  int nr;                                       // zmienna elementu
  komorka tmp;
  while(1){
    cout<<endl;                                 // menu wyboru
    cout<<"Obsluga listy osob."<<endl;
    cout<<"Wybierz co chcesz robic:"<<endl;
    cout<<"1. Dodaj do listy element"<<endl;
    cout<<"2. Wyswietl liste"<<endl;
    cout<<"3. Usun z listy element"<<endl;
    cout<<"4. Usun z listy wskazany element"<<endl;
    cout<<"5. Wyczysc liste"<<endl;
    cout<<"q. zamknij"<<endl;
    cin>>wybor;
    switch(wybor){
    case '1':
      cin>>tmp;
      lista.wstaw(tmp);
      break;
    case '2':
      wyswietl(lista);
      break;
    case '3':
      try{
	cout<<"Usunieto: "<<lista.usun()<<endl;
      }catch(ListEmptyException){
	cout<<"Lista jest pusta"<<endl;
      }
      break;
    case '4':
      cout<<"Ktory element chcesz usunac: ";
      cin>>nr;
      usun(lista,nr);
      break;
    case '5':
      lista.clear();
      cout<<endl<<"Lista zostala oprozniona"<<endl;
      break;
    case 'q':
      lista.clear(); // zwalnianie pamieci przed zamknieciem
      cout<<"dl: "<<lista.rozmiar()<<endl<<"Is empty: "<<lista.isEmpty()<<endl;
      cout<<"Wyciek pamieci: "<<dl_kolejki<<endl;
      return 0;
      break;
    default:
      cout<<"Zle okreslony wybor"<<endl;
      break;
    }
  }
}

ostream& operator << (ostream& StrmWy,const komorka &WyswietlanaWartosc){                        // wyswietlanie komorek
  std::cout<<"Imie: "<<WyswietlanaWartosc.imie<<endl<<"Wiek: "<<WyswietlanaWartosc.wiek<<endl;
  return StrmWy;
}

istream& operator >> (istream& StrmWy,komorka &dane){  // inicjalizowanie komorki
  std::cout<<std::endl;
  std::string napis;
  std::cout<<"Podaj imię: ";
  std::cin>>dane.imie;
  std::cout<<"Podaj wiek: ";
  std::cin>>dane.wiek;
  std::cout<<std::endl;
  return StrmWy;  
}

bool wyswietl(List<komorka> &dane){
  List<komorka> tmp;
  int rozmiar=dane.rozmiar();
  if(dane.isEmpty()){
    cout<<endl<<"Lista jest pusta"<<endl;
    return false;
  }
  for(int i=0;i<rozmiar;i++){     // przeniesienie danych na druga liste
    cout<<dane.pierwszy()<<endl;
    tmp.wstaw(dane.usun());
  }
  for(int i=0;i<rozmiar;i++){     // przeniesienie danych spowrotem
    dane.wstaw(tmp.usun());
  }
  tmp.clear();
  return true;
}

bool usun(List<komorka> &lista,int nr){
  List<komorka> tmp;
  if(nr>lista.rozmiar()){
    cout<<"podano element nie nalezacy do listy."<<endl<<"Lista ma "<<lista.rozmiar()<<" elementow"<<endl;
    return false;
  }
  for(int i=1;i<nr;i++){                            // przelozenie wszystkich poprzedzajacych elementow
    tmp.wstaw(lista.usun());
  }
  try{
    cout<<"Usunieto: "<<endl<<lista.usun()<<endl;   // usuniecie wskazanego elementu
  }catch(ListEmptyException){
    cout<<"Lista jest pusta"<<endl;
  }    
  for(int i=1;i<nr;i++){                            // nalozenie poprzedzajacych elementow spowrotem
    lista.wstaw(tmp.usun());
  }
  return true;
}
