#include <iostream>
#include <string>
#include "lista.hh"

using namespace std;

int dl_kolejki=0;
bool wyswietl(List<std::string> &dane);
void permutacje(string ciag,int liczba,List<std::string>& lista);         // generuje permutacje ciagu znakow. parametr liczba jest pomocniczym, nalezy go ustawic na 0 przy wywolaniu
bool jestPal(string testStr);                    // sprawdza czy podany ciag jest palindromem, jezeli tak to dodaje go do listy
void zamien(char &a,char &b){char c=a;a=b;b=c;}  // zamienia miejscami zmienne a oraz b
int usunDup(List<std::string>& lista);                                   // usuwa powtorzenie na liscie ciagow znakowych
List<std::string> palList;

int main(int argc, char *argv[]){    // glowna funkcja wywolujaca jestPal oraz usunDup
  string ciag_znakow;                // przechowuje wprowadzone dane
  cout<<endl<<"Podaj ciag znakow: ";
  cin>>ciag_znakow;
  permutacje(ciag_znakow,0,palList);
  usunDup(palList);
  wyswietl(palList);
  palList.clear();    // zwalnianie pamieci
  // cout<<"Wyciek pamieci: "<<dl_kolejki<<endl; //kontrola wycieku pamieci
  return 0;
}

bool wyswietl(List<std::string> &dane){
  List<std::string> tmp;
  int rozmiar=dane.rozmiar();
  for(int i=0;i<rozmiar;i++){         // przeniesienie na druga liste
    cout<<dane.pierwszy()<<endl;      // wyswietlenie
    tmp.wstaw(dane.usun());
  }
  for(int i=0;i<rozmiar;i++){         // przeniesienie elementow spowrotem
    dane.wstaw(tmp.usun());
  }
  tmp.clear();
  //  cout<<"dl: "<<tmp.rozmiar()<<endl<<"Is empty: "<<tmp.isEmpty()<<endl;
  //  cout<<"Wyciek pamieci: "<<dl_kolejki<<endl;
  return true;
}

int usunDup(List<std::string>& lista){
  int licznik=lista.rozmiar(); // orkesla dlugosc listy przed pozbyciem sie powtorzen
  int licznik_petli=0;  // zmienna pomagajaca obliczyc ile bylo powtorzen
  string pomocniczy;    // przechowuje chwilowo wartosc do porownania
  string *tablica=new string[licznik];  // przechowuje zawartosc listy na okres sprawdzania
  for(int i=0;i<licznik;i++){
    tablica[i]=lista.usun();              // przenoszenie danych z listy do tablicy
  }
  for(int i=0;i<licznik;i++){            // kasowanie powtorzen
    pomocniczy=tablica[i];
    for(int j=0;j<licznik;j++){
      if(j!=i)if(!(pomocniczy.empty()))if(pomocniczy==tablica[j]){
	    tablica[j].clear();
	  }
    }
  }
  for(int i=0;i<licznik;i++){            // przenoszenie danych z tablicy do listy
    if(!tablica[i].empty()){
      licznik_petli++;
      lista.wstaw(tablica[i]);
    }
  }
  delete[](tablica);                     // zwalnianie pamieci zajmowanej przez tablice
  return licznik-licznik_petli;
}

void permutacje(string ciag,int liczba,List<std::string>& lista){
  int dl=ciag.length();
  if(liczba<dl-1){
    for(int i=liczba;i<dl;i++){
      zamien(ciag[liczba],ciag[i]);
      permutacje(ciag,liczba+1,lista);     // wywolanie rekurencyjne
      zamien(ciag[liczba],ciag[i]);
    }
  }
  else{
    //    cout<<ciag<<endl;    // wyswietlenie permutacji
    if(jestPal(ciag)){         // jezeli ciag powstaly w permutacji jest polindromem to zostaje dodany do listy
      lista.wstaw(ciag); 
    }
  }
}

bool jestPal(string testStr){
  int dl=testStr.length();
  if(dl==1)return true; // ciag o jednym znaku jest palindromem
  else if(dl==2){       // sprawdzenie siagu dwuznakowych
      if((testStr[0]==testStr[1])||(testStr[0]==testStr[1]+32)||(testStr[0]==testStr[1]-32))return true;
      else return false;
    }
  if(!((testStr[0]==testStr[dl-1])||(testStr[0]==testStr[dl-1]+32)||(testStr[0]==testStr[dl-1]-32)))return false; // sprawdzenie elementow skrajnych
  if(!(jestPal(testStr.substr(1,dl-2))))return false;  // wywolanie rekurencyjne
  return true;
}
