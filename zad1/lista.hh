class ListEmptyException{};
extern int dl_kolejki; // zmienna do okreslania przecieku pamieci

template <class Object>
class wezelListy{
  Object zawartosc;    // dane
  wezelListy<Object> *next; // wskaznik do przodu
public:
  wezelListy();
  ~wezelListy();
  Object& getZawartosc();
  Object& setZawartosc(const Object &dane);
  wezelListy<Object>*& getNext();
  wezelListy<Object>*& setNext(wezelListy<Object> (*dane));
};

template <class Object>
Object& wezelListy<Object>::getZawartosc(){return zawartosc;}

template <class Object>
Object& wezelListy<Object>::setZawartosc(const Object &dane){zawartosc=dane;return zawartosc;}

template <class Object>
wezelListy<Object>*& wezelListy<Object>::getNext(){return next;}

template <class Object>
wezelListy<Object>*& wezelListy<Object>::setNext(wezelListy<Object> (*dane)){next=dane;return next;}

template <class Object>
wezelListy<Object>::~wezelListy(){dl_kolejki--;}

template <class Object>
wezelListy<Object>::wezelListy(){next=NULL;dl_kolejki++;}

template <class Object> // szablon listy
class List{
  Object zawartosc;    // dane
  wezelListy<Object> *head; // wskaznik do przodu  
  int dl;
public:
  List();
  int rozmiar() const;
  bool isEmpty() const; 
  bool clear();         // wyczysc
  Object& pierwszy() throw(ListEmptyException);
  Object& wstaw(const Object& obj);
  Object& usun() throw(ListEmptyException);
};

template <class Object>
List<Object>::List(){
  head=NULL;
  dl=0;
}

template <class Object>
int List<Object>::rozmiar() const{
  return dl;
}

template <class Object>
bool List<Object>::isEmpty() const{
  if(dl) return false;
  return true;
}

template <class Object>
bool List<Object>::clear(){
  wezelListy<Object> *tmp=NULL;
  while(head!=NULL){ // dopoki sa elementy
    tmp=head->getNext();  // przejscie na nastepne elementy
    //    cout<<"Usunieto znak: "<<next->zawartosc<<endl; // kontrola dzialania funkcji
    delete(head); // zaolnienie pamieci
    head=tmp; // przesuniecie korzenia
    dl=0;
  }
  if(head==NULL){
    //    std::cout<<"Lista jest pusta"<<std::endl; // kontrola dzialania funkcji
    return true;
  }
  return false;
}

template <class Object>
Object& List<Object>::pierwszy() throw(ListEmptyException){
  if(head!=NULL){
    return head->getZawartosc(); // zwrocenie wartosci pierwszego elementu
  }else{
    //cout<<"Lista jest pusta"<<endl;
    throw ListEmptyException(); // zrzucenie bledu
  }
}

template <class Object>
Object& List<Object>::wstaw(const Object& obj){
  wezelListy<Object> *tmp=NULL;
  tmp=head;                      // zapamietanie oczatku
  head=new wezelListy<Object>;   // zaalokowanie nowego wezla
  head->setZawartosc(obj);
  dl++;    
  head->setNext(tmp);            // powiekszenie listy
  return head->getZawartosc();
}

template <class Object>
Object& List<Object>::usun() throw(ListEmptyException){
  wezelListy<Object> *tmp=NULL;
  if(head!=NULL){ // gdy lista zawiera elementy
    tmp=head->getNext();
    //    cout<<"Usunieto znak: "<<next->zawartosc<<endl;
    zawartosc=head->getZawartosc();
    delete(head); // zwolnienie pamieci
    head=tmp;
    dl--;
    return zawartosc;
  }else{ // gdy lista pusta
    //cout<<"Lista jest pusta"<<endl;
    throw ListEmptyException(); // zrzucenie bledu 
  }
}
